FROM debian:bullseye-slim

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Europe/Moscow

ADD clone.sh .
ADD askpass.sh .

RUN chmod +x clone.sh && \
    chmod +x askpass.sh && \
    apt-get update && \
    apt-get -y --quiet --no-install-recommends install \
      ca-certificates \
      git \
      tree && \
    apt-get clean && \
    mkdir -p /git


ENTRYPOINT ["/bin/bash", "clone.sh"]


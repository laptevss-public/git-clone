# git-clone

Clones the specified repository branch using HTTPS

# Environment variables

GIT_REPO_URL - path to repo (for ex., https://gitlab.com/mycompany/my-repo.git )

GIT_BRANCH - clone branch, by default main

GIT_OUTPUT_DIR - folder to be cloned to, by default /git

GIT_USERNAME - git username

GIT_PASSWORD - git assword or token

# Example (from docker registry)
```
docker run -it --rm -e GIT_USERNAME=my_login -e GIT_PASSWORD=my_pw -e GIT_REPO_URL=https://gitlab.com/laptevss-public/git-clone.git -e GIT_BRANCH=main -e GIT_OUTPUT_DIR=/tmp laptevss/git-clone
```

# Example (from gitlab registry)
```
docker run -it --rm -e GIT_USERNAME=gitlab+deploy-token-123456 -e GIT_PASSWORD=my_token -e GIT_REPO_URL=https://gitlab.com/laptevss-public/git-clone.git -e GIT_BRANCH=main -e GIT_OUTPUT_DIR=/git registry.gitlab.com/laptevss-public/git-clone
```
